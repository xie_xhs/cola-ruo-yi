package org.xhs.study.ruoyi.system.valueobject;

/**
 * @author xiehusheng
 * @code 2023/10/20
 */
public enum SexEnum {

    /**
     * 女 0
     */
    FEMALE(0,"女"),
    /**
     * 男  1
     */
    MALE(1,"男"),
    /**
     * 未知 9
     */
    UNKNOWN(9,"未知");

    SexEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public final int code;
    public final String desc;

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
