package org.xhs.study.ruoyi.system.valueobject;

/**
 * @author xiehusheng
 * @code 2023/10/20
 */
public enum AccountStatus {
    /**
     * 正常 - 0
     */
    NORMAL(0, "正常"),

    /**
     * 停用 - 1
     */
    DEACTIVATE(1, "停用");
    public final int code;
    public final String desc;

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    AccountStatus(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
